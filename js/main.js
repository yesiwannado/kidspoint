window.$ = window.jQuery = window.jquery = require('jquery');
require('malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min')($);
window.WOW = require('wowjs').WOW;
require('owl.carousel');
require('EasyTabs/lib/jquery.easytabs.min');
require('air-datepicker/dist/js/datepicker');


const bodyScrollLock = require('body-scroll-lock');
const disableBodyScroll = bodyScrollLock.disableBodyScroll;
const enableBodyScroll = bodyScrollLock.enableBodyScroll;

const targetElement = document.querySelector(".tab-container");

$(document).ready(function() {

    new WOW().init();

    $('html, body, .block').click(function () {
        $('#searchResults').fadeOut(200);
        $('#citySelect').fadeOut(200);
        $('#categSort').fadeOut(200);
        $('.city-defined').removeClass('opened');
        $('#sortDef').removeClass('opened');
        $('.header-search-block .common-input, .header-search-block').removeClass('visible')
    })

    // Filter Tabs
        $('.tab-container').easytabs();

    // Scroll statements

    $(window).scroll(function() {

        var y = $(this).scrollTop();
        if (y > 50) {
            $('.page-header').addClass('shrinked');
            $('.main-menu-block').addClass('upper');
        }
        else {
            $('.page-header').removeClass('shrinked');
            $('.main-menu-block').removeClass('upper');
        }
    });



    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 'onLeave'}});

    var scene = new ScrollMagic.Scene({triggerElement: '.categ-filter-header'})
        .setPin('.categ-filter-header')
        .addTo(controller);

    var sceneTwo = new ScrollMagic.Scene({
        triggerElement: '.categ-filter-header'
    })
        .setClassToggle('.categ-filter-header', 'inview')
        .addTo(controller);

    var sceneThree = new ScrollMagic.Scene({
        triggerElement: '.see-also'
    })
        .setClassToggle('.categ-filter-header', 'false-hidden')
        .addTo(controller)

    $('.open-filters').click(function (of) {
        $('.tab-container').toggleClass('opened')
        $('.categ-filter-header').toggleClass('opened');
        $('.apply-reset-filters').toggleClass('visible')
        $('.filter-darkner').fadeToggle(300);
        $(this).text(function(i, text){
            return text === "Открыть фильтры" ? "Свернуть фильтры" : "Открыть фильтры";
        })
    })

    // Index preloader

    $('.preloader').delay(1000).fadeOut(300);

    $(window).scroll(function() {

        var y = $(this).scrollTop();
        if (y > 10) {
            $('.page-header').addClass('ontop');
        }
        else {
            $('.page-header').removeClass('ontop')
        }
    });

    // Index Filters

    $('.indexWhen').select2({
        placeholder: "Где?"
    });
    $('.indexWhat').select2({
        placeholder: "Что делать",
        minimumResultsForSearch: -1
    });
    $('.indexAge').select2({
        placeholder: "Возраст",
        minimumResultsForSearch: -1
    });
    $('.indexMetro').select2({
        placeholder: "Станция метро",
        minimumResultsForSearch: -1
    });
    $('select').on('select2:open', function (e) {
        $('.select2-results__options').mCustomScrollbar('destroy');
        setTimeout(function () {
            $('.select2-results__options').mCustomScrollbar();
        }, 0);
    });


    // Header actions

    $( "#headerSearch" ).on('keyup', function() {
        $('#searchResults').fadeIn(300);
        $('.search-wrapper').removeClass('error');
    });

    $('.city-defined').click(function (citySelect) {
        citySelect.stopPropagation();
        $(this).toggleClass('opened');
        $('#citySelect').fadeToggle(300);
    })

    $('#sortDef').click(function (citySelect) {
        citySelect.stopPropagation();
        $(this).toggleClass('opened');
        $('#categSort').fadeToggle(300);
    })

    $('.yes, #mscRegion').click(function () {
        $('.city-def-phrase').text('Ваш регион:');
        $('.yesno').addClass('hidden');
        $('.city-defined.msc').addClass('visible');
        $('.city-defined.other').removeClass('visible');
    })
    $('.no, #otherRegion').click(function () {
        $('#otherCityPopup').fadeIn(300);
        $('.city-defined.other').addClass('visible');
        $('.city-defined.msc').removeClass('visible');
        $('.city-def-phrase').text('Ваш регион:');
        $('.yesno').addClass('hidden');
    })



    // Popups

    $('.popup-wrapper, .close-popup, .just-close-btn').click(function () {
        $('.popup-wrapper').fadeOut(300);
    })
    $('.popup').click(function (popupAction) {
        popupAction.stopPropagation();
    })

    $('.gosearch').click(function (noResults) {
        noResults.preventDefault();
        $('#noSearchResults').fadeIn(300);
    });

    $('#comclose').click(function (comClose) {
        comClose.preventDefault();
        $('#comclosePopup').fadeIn(300);
    });

    $('.submit-fb').click(function (submitFb) {
        submitFb.preventDefault();
        $('#swearPopup').fadeIn(300);
    });

    $('.getpres').click(function (submitFb) {
        submitFb.preventDefault();
        $('#contactUsPopup').fadeIn(300);
    });

    $('#submitCont').click(function (sc) {
        sc.preventDefault();
        sc.stopPropagation();
        $('.contact-us-popup h3').text('Спасибо за интерес!');
        $('.contact-us-popup').addClass('formsent');
    })

    // Categ list-map

    $('.map-btn').click(function () {
        $('.categ-cards-block').addClass('categ-map');
        $('body').addClass('locked');
        $('.main-menu-block').addClass('hidden');
        $('.page-header').addClass('hidden');
        $('.categ-filter-header').addClass('fixed');
        $('.inview').addClass('onmap');
        $(this).addClass('active');
        $('.list-btn').removeClass('active')
    })

    $('.list-btn').click(function () {
        $('.categ-cards-block').removeClass('categ-map');
        $(this).addClass('active');
        $('.map-btn').removeClass('active')
        $('body').removeClass('locked');
        $('.main-menu-block').removeClass('hidden');
        $('.page-header').removeClass('hidden');
        $('.categ-filter-header').removeClass('fixed');
        $('.inview').removeClass('onmap');
    })

    $('.categ-cards-block').click(function() {
        $('.categ-cards-block').removeClass('categ-map');
        $('body').removeClass('locked');
        $('.main-menu-block').removeClass('hidden');
        $('.page-header').removeClass('hidden');
        $('.categ-filter-header').removeClass('fixed');
        $('.list-btn').addClass('active');
        $('.map-btn').removeClass('active');
        $('.inview').removeClass('onmap')
    })

    $('.map-scroller, .map-itself').click(function(ee) {
        ee.stopPropagation();
    })

    $('.marker').click(function () {
        $('#hig').addClass('highlight');
    })

    // Feedbacks

    $('.leavereply').click(function () {
        $('.leaving-feedback').addClass('visible');
        var id = $(this).attr('href'),
            top = $(id).offset().top - 240;
        $('body,html').animate({scrollTop: top}, 1500);
    })

    $('.showrep').click(function () {
        $('.hidden-replies').fadeToggle(300);
        $(this).text(function(i, text){
            return text === "Показать все ответы" ? "Скрыть ответы" : "Показать все ответы";
        })
    })

    $('.yml').click(function () {
        $('.yml').removeClass('active');
        $(this).addClass('active');
    })

    $(".cc-benefits").mCustomScrollbar({
        axis:"x",
        advanced:{ autoExpandHorizontalScroll:true }
    });

    $(".search-results-block").mCustomScrollbar({
        axis:"y"
    });

    $('.many-filters-list li').click(function () {
        $(this).toggleClass('active');
    })

    $('.slider-input').jRange({
        from: 0,
        to: 300000,
        step: 1,
        format: '%s',
        width: 400,
        showLabels: true,
        isRange : true
    });

    /*It's all about mobile*/

    $('.mobile-search-btn').click(function (mobSearch) {
        mobSearch.stopPropagation();
        $('.header-search-block .common-input, .header-search-block').toggleClass('visible').focus();
    })

    if ($(window).width() < 650) {
        $('.city-def-phrase').text('Ваш регион:');
        $('.open-filters').text('');

        $(".main-menu-block").mCustomScrollbar({
            axis:"x",
            advanced:{ autoExpandHorizontalScroll:true }
        });

        $(".tab-container").mCustomScrollbar({
            axis:"y"
        });

        $('.open-filters').click(function () {
            $(this).text(function(i, text){
                return text === "" ? "" : "";
            })
            $(this).toggleClass('toclose');
            var clicks = $(this).data('clicks');
            if (clicks) {
                enableBodyScroll(targetElement);
            }
            else {
                disableBodyScroll(targetElement);
            }
            $(this).data("clicks", !clicks);
        })
    }

});


document.addEventListener("touchstart", function(){}, true);